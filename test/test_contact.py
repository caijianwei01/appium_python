import time
import os

import allure
from appium import webdriver
from loguru import logger
from yaml import safe_load

timeout = 10
poll = 1

base_path = os.path.dirname(os.path.dirname(__file__))
yml_path = os.path.join(base_path, 'desc_caps.yml')
app_path = os.path.join(base_path, 'app', 'ContactManager.apk')


@allure.feature('添加联系人')
class TestContact:

    def setup(self):
        with open(yml_path, encoding='utf-8') as f:
            data_dict = safe_load(f)

        desired_caps = data_dict['desired_caps']
        desired_caps.update(app=app_path)
        self.driver = webdriver.Remote(data_dict['appium_server_url'], desired_caps)
        self.driver.implicitly_wait(5)

    def teardown(self):
        self.driver.quit()

    @allure.title('添加联系人_成功')
    def test_contact(self):
        """  """

        # Workaround for version issue
        self._click_confirm_ok_btn()

        self._click_add_contact_btn()
        self._input_contact_name('A san')
        self._input_email('asan@example.com')
        self._click_save_btn()

        # Workarount for version issue
        self._click_confirm_ok_btn()

    def _click_add_contact_btn(self):
        elem = self._find_elem_by_xpath('//android.widget.Button[contains(@resource-id,"addContactButton")]')
        logger.info('Click add contact button')
        elem.click()

    def _input_contact_name(self, txt_name):
        elem = self._find_elem_by_xpath('//android.widget.EditText[contains(@resource-id, "contactNameEditText")]')
        logger.info(f'输入联系人名称: {txt_name}')
        elem.send_keys(txt_name)

    def _input_email(self, txt_email):
        elem = self._find_elem_by_xpath('//android.widget.EditText[contains(@resource-id, "contactEmailEditText")]')
        logger.info(f'输入邮箱地址: {txt_email}')
        elem.send_keys(txt_email)

    def _click_save_btn(self):
        elem = self._find_elem_by_xpath('//android.widget.Button[contains(@resource-id, "contactSaveButton")]')
        logger.info('点击保存按钮')
        elem.click()

    def _click_confirm_ok_btn(self):
        elem = self._find_elem_by_xpath('//android.widget.Button[contains(@resource-id, "android:id/button1")]',
                                        time_out=3, raise_exception=False)
        if elem is not None:
            logger.info('Click the ok button on confirm dialog')
            elem.click()
        else:
            logger.info('No confirm dialog found')

    def _find_elem_by_xpath(self, elem_xpath, time_out=timeout, raise_exception=True):
        start = time.time()
        elem = None
        while time.time() - start < time_out and elem is None:
            time.sleep(poll)
            try:
                elem = self.driver.find_element_by_xpath(elem_xpath)
            except Exception:
                logger.info('by pass the element not found')

        if elem is None and raise_exception:
            raise LookupError(f'The element which xpath is {elem_xpath} could not be found')

        return elem
